import styled, { css } from 'styled-components'

export const Wrapper = styled.div`
  background: black;
  border-radius: 8px;
  margin: 16px auto;
  overflow: hidden;
  pointer-events: none;
  display: flex;
  justify-content: center;
`

export const Tile = styled.div`
  width: 100%;
  height: 100%;
  border: 4px solid transparent;
  border-radius: 8px;
  box-sizing: border-box;
  transition: all 0.1s ease-in;
  overflow: hidden;
  ${(props) =>
    props.isSelected &&
    css`
      border-color: white;
      transform: scale(1.02);
    `}
`

export const StyledImg = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`
