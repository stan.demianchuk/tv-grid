import { observer, Observer } from 'mobx-react-lite'
import Grid from '../Grid'
import { useStore } from '../../store-context'
import { Wrapper, Tile, StyledImg } from './styled'

const PhotoGallery = observer(() => {
  const store = useStore()
  return (
    <Wrapper>
      <Grid
        width={1280}
        height={720}
        columnsCount={5}
        rowHeight={320}
        renderRange={50}
        overscanCount={2}
        onItemsRendered={store.loadPhotos}
      >
        {({ key, style, index, isSelected }) => (
          <div key={key} style={{ ...style, padding: '8px' }}>
            <Tile isSelected={isSelected}>
              <Observer>
                {() => {
                  if (store.photos.length <= index) return null
                  return <StyledImg src={store.photos[index]} alt="" />
                }}
              </Observer>
            </Tile>
          </div>
        )}
      </Grid>
    </Wrapper>
  )
})

export default PhotoGallery
