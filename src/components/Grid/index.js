import { useEffect, useReducer, useRef, useLayoutEffect } from 'react'
import { useDebouncedCallback } from 'use-debounce'
import usePrev from '../../hooks/usePrev'
import useKeyPress from '../../hooks/useKeyPress'
import easeInOut from '../../helpers/easeInOut'

const UPDATE_DIMENSIONS = 'UPDATE_DIMENSIONS'
const SELECT_NEXT_COLUMN = 'SELECT_NEXT_COLUMN'
const SELECT_PREV_COLUMN = 'SELECT_PREV_COLUMN'
const SELECT_NEXT_ROW = 'SELECT_NEXT_ROW'
const SELECT_PREV_ROW = 'SELECT_PREV_ROW'
const SET_ROW_START_INDEX = 'SET_ROW_START_INDEX'
const END_SCROLL = 'END_SCROLL'

const initialState = {
  columnWidth: 0,
  rowHeight: 0,
  columnsCount: 0,
  rowsCount: 0,
  selectedColumnIndex: 0,
  selectedRowIndex: 0,
  rowStartIndex: 0,
  isScrolling: false,
  scrollDirection: 'none',
}

const reducer = (state, action) => {
  switch (action.type) {
    case UPDATE_DIMENSIONS: {
      const { width, columnsCount, renderRange, rowHeight } = action.payload
      return {
        ...state,
        columnsCount,
        rowHeight,
        rowsCount: Math.ceil(renderRange / columnsCount),
        columnWidth: width / columnsCount,
      }
    }
    case END_SCROLL: {
      const { scrollDirection, selectedRowIndex, rowStartIndex, rowsCount } = state
      let nextRowStartIndex = rowStartIndex
      if (scrollDirection === 'down' && selectedRowIndex + 2 >= rowStartIndex + rowsCount) {
        nextRowStartIndex = selectedRowIndex
      }
      if (scrollDirection === 'up' && selectedRowIndex <= rowStartIndex + 1) {
        nextRowStartIndex = rowStartIndex - rowsCount + 3
      }

      return { ...state, isScrolling: false, scrollDirection: 'none', rowStartIndex: Math.max(0, nextRowStartIndex) }
    }
    case SELECT_NEXT_COLUMN: {
      const { selectedColumnIndex, columnsCount } = state
      if (selectedColumnIndex === columnsCount - 1) {
        return state
      }
      return { ...state, selectedColumnIndex: selectedColumnIndex + 1 }
    }
    case SELECT_PREV_COLUMN: {
      return { ...state, selectedColumnIndex: Math.max(0, state.selectedColumnIndex - 1) }
    }
    case SELECT_NEXT_ROW: {
      if (state.isScrolling) {
        return state
      }
      return { ...state, isScrolling: true, scrollDirection: 'down', selectedRowIndex: state.selectedRowIndex + 1 }
    }
    case SELECT_PREV_ROW: {
      if (state.isScrolling) return state
      return {
        ...state,
        isScrolling: true,
        scrollDirection: 'up',
        selectedRowIndex: Math.max(0, state.selectedRowIndex - 1),
      }
    }
    case SET_ROW_START_INDEX: {
      return { ...state, rowStartIndex: Math.max(0, action.payload) }
    }
    default: {
      // throw new Error(action.type)
      return state
    }
  }
}

const scrollTo = (element, to, duration) => {
  let currentTime = 0
  const start = element.scrollTop
  const change = to - start
  const increment = 10

  const animateScroll = () => {
    currentTime += increment
    // eslint-disable-next-line no-param-reassign
    element.scrollTop = easeInOut(currentTime, start, change, duration)
    if (currentTime <= duration) {
      // return promRaf(animateScroll)
      return new Promise(requestAnimationFrame).then(animateScroll)
    }
    // eslint-disable-next-line no-param-reassign
    element.scrollTop = to
    return Promise.resolve()
  }
  return animateScroll()
}

const getCellIndex = ({ columnIndex, rowIndex, columnsCount }) => rowIndex * columnsCount + columnIndex

const getRenderedIndices = ({ rowStartIndex, columnsCount, rowsCount }) => {
  const startCellIndex = getCellIndex({ rowIndex: rowStartIndex, columnsCount, columnIndex: 0 })
  const stopCellIndex = startCellIndex + columnsCount * rowsCount
  return [startCellIndex, stopCellIndex]
}

const Grid = ({ width, height, columnsCount, rowHeight, renderRange, onItemsRendered, children: renderCell }) => {
  const [state, dispatch] = useReducer(reducer, initialState)
  const scrollerRef = useRef()
  const { selectedColumnIndex, selectedRowIndex, rowsCount, columnWidth, rowStartIndex } = state
  const selectedCellIndex = getCellIndex({ columnIndex: selectedColumnIndex, rowIndex: selectedRowIndex, columnsCount })
  const renderedIndices = getRenderedIndices({ rowStartIndex, columnsCount, rowsCount })
  const prevRenderedIndices = usePrev(renderedIndices, [0, 0])

  const cellStyles = {
    width: columnWidth,
    height: rowHeight,
    boxSizing: 'border-box',
    transform: `translateY(${rowStartIndex * rowHeight}px)`,
  }

  const renderedCells = (() => {
    const cells = []
    const [start, stop] = renderedIndices
    for (let cellIndex = start; cellIndex < stop; cellIndex += 1) {
      cells.push({
        isSelected: cellIndex === selectedCellIndex,
        key: cellIndex,
        index: cellIndex,
        style: cellStyles,
      })
    }
    return cells
  })()

  // Derrive state from props:
  useEffect(() => {
    dispatch({
      type: UPDATE_DIMENSIONS,
      payload: { width, height, columnsCount, renderRange },
    })
  }, [width, height, columnsCount, renderRange])

  // Run onItemsRendered hook when new cells are rendered:
  useEffect(() => {
    if (typeof onItemsRendered === 'function') {
      const [, stop] = renderedIndices
      const [, prevStop] = prevRenderedIndices
      if (stop > prevStop) {
        onItemsRendered(prevStop, stop)
      }
    }
  }, [onItemsRendered, renderedIndices, prevRenderedIndices])

  // Set up scroll animation:
  useLayoutEffect(() => {
    if (scrollerRef.current) {
      scrollTo(scrollerRef.current, selectedRowIndex * rowHeight, 500).then(() => {
        dispatch({ type: END_SCROLL })
      })
    }
  }, [selectedRowIndex, rowHeight])

  // Event handlers:
  const handleWheel = ({ deltaX, deltaY }) => {
    const axis = Math.abs(deltaX) - Math.abs(deltaY) > 0 ? 'x' : 'y'
    if (axis === 'x') {
      if (deltaX > 0) {
        dispatch({ type: SELECT_NEXT_COLUMN })
      } else {
        dispatch({ type: SELECT_PREV_COLUMN })
      }
    }
    if (axis === 'y') {
      if (deltaY > 0) {
        dispatch({ type: SELECT_NEXT_ROW })
      } else {
        dispatch({ type: SELECT_PREV_ROW })
      }
    }
  }
  const debouncedHandleWheel = useDebouncedCallback(handleWheel, 50, { leading: true, trailing: false })

  useEffect(() => {
    window.addEventListener('wheel', debouncedHandleWheel)
    return () => window.removeEventListener('wheel', debouncedHandleWheel)
  }, [debouncedHandleWheel])

  useKeyPress('ArrowLeft', () => dispatch({ type: SELECT_PREV_COLUMN }))
  useKeyPress('ArrowRight', () => dispatch({ type: SELECT_NEXT_COLUMN }))
  useKeyPress('ArrowUp', () => dispatch({ type: SELECT_PREV_ROW }))
  useKeyPress('ArrowDown', () => dispatch({ type: SELECT_NEXT_ROW }))

  return (
    <div
      ref={scrollerRef}
      style={{
        width,
        height,
        minWidth: width,
        overflow: 'hidden',
        position: 'relative',
      }}
    >
      <div
        style={{
          width: '100%',
          height: `9999px`,
        }}
      >
        <div
          style={{
            width: '100%',
            height: `${rowsCount * rowHeight}px`,
            position: 'relative',
            display: 'flex',
            flexWrap: 'wrap',
          }}
        >
          {renderedCells.map(renderCell)}
        </div>
      </div>
    </div>
  )
}
export default Grid
