import { useEffect, useState } from 'react'

export default function useKeyPress(targetKey, onPressDown = () => {}, onPressUp = () => {}) {
  const [keyPressed, setKeyPressed] = useState(false)

  useEffect(() => {
    const downHandler = (event) => {
      if (event.key === targetKey) {
        setKeyPressed(true)
        onPressDown(event)
      }
    }

    const upHandler = (event) => {
      if (event.key === targetKey) {
        setKeyPressed(false)
        onPressUp(event)
      }
    }

    window.addEventListener('keydown', downHandler)
    window.addEventListener('keyup', upHandler)

    return () => {
      window.removeEventListener('keydown', downHandler)
      window.removeEventListener('keyup', upHandler)
    }
  })

  return keyPressed
}
