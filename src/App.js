import { StoreProvider } from './store-context'
import PhotoGallery from './components/PhotoGallery'

const App = () => {
  return (
    <StoreProvider>
      <PhotoGallery />
    </StoreProvider>
  )
}
export default App
