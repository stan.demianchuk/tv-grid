import { flow, observable, makeObservable } from 'mobx'
import FlickrApi from '../api/Flickr'

export default class PhotoStore {
  photos = []

  constructor() {
    makeObservable(this, {
      photos: observable,
      loadPhotos: flow,
    })

    this.loadPhotos = this.loadPhotos.bind(this)
  }

  *loadPhotos(from, to) {
    try {
      const newPhotos = yield FlickrApi.getPhotos(to - from, from + 1)
      this.photos = [...this.photos, ...newPhotos]
    } catch (message) {
      // eslint-disable-next-line no-console
      console.error(message)
    }
  }
}
