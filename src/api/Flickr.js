const API_KEY = '1f800c374aa0ea80b425bfc8f8cbad65'
const getImgUrl = ({ id, server, secret }) => `https://live.staticflickr.com/${server}/${id}_${secret}_c.jpg`
export default class Flickr {
  static getPhotos(limit, page) {
    return fetch(
      `https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=${API_KEY}&per_page=${limit}&page=${page}&format=json&nojsoncallback=1`,
    )
      .then((response) => response.json())
      .then((response) => response.photos.photo.map(getImgUrl))
  }
}
