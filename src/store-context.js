import { createContext, useContext } from 'react'
import PhotoStore from './stores/PhotosStore'

const StoreContext = createContext({})

export const useStore = () => useContext(StoreContext)
export const StoreProvider = ({ children }) => (
  <StoreContext.Provider value={new PhotoStore()}>{children}</StoreContext.Provider>
)
